# README #

Este README documenta as características principais deste repositório:

* Estudos FTA (Framework de Testes Automáticos)

### Para que serve este repositório? ###

É uma ferramenta pedagógica, e trará uma configuração inicial para diversos tipos de projeto que utilizam testes funcionais automáticos

### Como eu posso instanciar o FTA? ###

Aconselhamos que as você tenha instalado no seu ambiente de desenvolvimento:

* Eclipse IDE ([link](https://www.eclipse.org/downloads/))
* Java JDK 1.8 ([link](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html))

### Principal contribuição ###

* Disponibilização de __framework__ de testes funcionais automáticos.

### Tecnologias Utilizadas ###

* Java ([link](https://www.oracle.com/java/index.html))
* Selenium ([link](https://www.seleniumhq.org))
* Cucumber ([link](https://cucumber.io/))
* Serenity BDD ([link](https://www.thucydides.info/))
* Maven ([link](https://maven.apache.org/))

### Contato ###

* [Prof.º Carlos Diego Quirino Lima](mailto:diegoquirino@gmail.com)

***
_[Aprenda mais sobre a linguagem de marcação utilizada](https://bitbucket.org/tutorials/markdowndemo)_