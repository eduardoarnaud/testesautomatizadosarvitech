package br.jus.tjpb.testes.fta.estudos.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InicialPageObject extends PageObject {

	@FindBy(id="welcome.button.contato")
	private WebElement botaoContato;
	
	public InicialPageObject(WebDriver driver) {
		super(driver);
	}
	
	public ContatoPageObject acessarPaginaContato() {
		this.clicarNoBotao(botaoContato);
		ContatoPageObject contato = new ContatoPageObject(this.getDriver());
		PageFactory.initElements(this.getDriver(), contato);
		return contato;
	}

}
