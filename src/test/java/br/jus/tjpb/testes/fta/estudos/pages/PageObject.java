package br.jus.tjpb.testes.fta.estudos.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PageObject {
	
	private WebDriver driver;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public PageObject(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public void clicarNoBotao(WebElement e) {
		e.click();
	}
	
	public void preencherCampo(WebElement e, String texto) {
		e.clear();
	    e.sendKeys(texto);
	}
	
	public void selecionar(WebElement e, String textoVisivel) {
		new Select(e).selectByVisibleText(textoVisivel);
	}

}
