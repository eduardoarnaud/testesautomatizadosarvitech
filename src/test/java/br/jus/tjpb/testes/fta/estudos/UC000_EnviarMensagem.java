package br.jus.tjpb.testes.fta.estudos;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import br.jus.tjpb.testes.fta.estudos.categories.AllSystem;
import br.jus.tjpb.testes.fta.estudos.categories.ProfileUser;
import br.jus.tjpb.testes.fta.estudos.pages.ContatoPageObject;
import br.jus.tjpb.testes.fta.estudos.pages.InicialPageObject;
import br.jus.tjpb.testes.fta.estudos.utils.DadosUtils;

@Category({AllSystem.class,ProfileUser.class})
public class UC000_EnviarMensagem {

	private WebDriver driver;
	private InicialPageObject paginaInicial;
	private ContatoPageObject paginaContato;

	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		paginaInicial = new InicialPageObject(driver);
		PageFactory.initElements(driver, paginaInicial);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	// TIPO MENSAGEM: DÚVIDA ---
	
	@Test
	public void testContatoDuvidaMenor18Anos() {
		driver.get(DadosUtils.getString("baseUrl"));
		
		paginaContato = paginaInicial.acessarPaginaContato();
		
		paginaContato.informarNome("Carlos Diego");
		paginaContato.informarEmail("carlos.lima@gmail.com");
		paginaContato.informarMensagem("Dúvida de Teste");
		paginaContato.escolherTipoMsg("duvida");
		paginaContato.selecionarIdade("Menor que 18 anos");
		paginaContato.enviar();
		
	    assertEquals("Mensagem enviada com sucesso!", paginaContato.getSucessoFalhaMensagem());
	  
	}
	
	
	// TIPO MENSAGEM: SUGESTÃO
	
	// TIPO MENSAGEM: RECLAMAÇÃO
	@Test
	public void testContatoReclamacaoDe18a65() {
		driver.get(DadosUtils.getString("baseUrl"));
		
		paginaContato = paginaInicial.acessarPaginaContato();
		
		paginaContato.informarNome("Carlos Diego");
		paginaContato.informarEmail("carlos.lima@gmail.com");
		paginaContato.informarMensagem("Reclamando de um problema");
		paginaContato.escolherTipoMsg("reclamacao");
		paginaContato.selecionarIdade("Entre 18 e 60 anos");
		paginaContato.enviar();
		
	    assertEquals("Mensagem enviada com sucesso!", paginaContato.getSucessoFalhaMensagem());
	  
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
