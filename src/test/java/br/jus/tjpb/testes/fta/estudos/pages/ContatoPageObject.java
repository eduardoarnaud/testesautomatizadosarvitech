package br.jus.tjpb.testes.fta.estudos.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ContatoPageObject extends PageObject {

	@FindBy(id="nome")
	private WebElement campoNome;
	
	@FindBy(id="email")
	private WebElement campoEmail;
	
	@FindBy(name="tipoMsg")
	private List<WebElement> radioTipoMsg;
	
	@FindBy(id="idade")
	private WebElement selectIdade;
	
	@FindBy(id="mensagem")
	private WebElement campoMensagem;
	
	@FindBy(id="contato.button.enviarMensagem")
	private WebElement botaoEnviarSuaMensagem;
    
	@FindBy(css="strong")
	private WebElement sucessoFalhaMensagem;
	
	public ContatoPageObject(WebDriver driver) {
		super(driver);
	}
	
	public void informarNome(String nome) {
		this.preencherCampo(campoNome, nome);
	}
	public void informarEmail(String email) {
		this.preencherCampo(campoEmail, email);
	}
	public void informarMensagem(String mensagem) {
		this.preencherCampo(campoMensagem, mensagem);
	}
	public void escolherTipoMsg(String tipo) {
		for(WebElement e: radioTipoMsg) {
			if(e.getAttribute("value").equalsIgnoreCase(tipo)) {
				e.click();
			}
		}
	}
	public void selecionarIdade(String faixa) {
		this.selecionar(selectIdade, faixa);
	}
	public void enviar() {
		botaoEnviarSuaMensagem.submit();
	}
	public String getSucessoFalhaMensagem() {
		return sucessoFalhaMensagem.getText();
	}

}
